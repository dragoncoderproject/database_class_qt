#include "database.h"

namespace DATABASE {

    bool Database::init () {
        QMessageBox message;
            database.setHostName(databaseHost);
            database.setDatabaseName(databaseName);
            database.setConnectOptions(databaseConnectionOptions);
            database.setUserName(databaseUserName);
            database.setPassword(databasePassword);
            database.setPort(databasePort);
            opened = database.open ();
        if (opened == false) {
            message.setText("Problem with connection. Check out if the name, password etc. are correct");
            message.exec();
        } else {

        }
        return opened;
    }

    QVector <QStringList> Database::getDatabaseNamesOnServer () {
        QMessageBox message;
        QVector <QStringList> list;
        QStringList databaseNames;
        if (getOpened() == true) {
            query.prepare("SELECT name FROM master.dbo.sysdatabases;");
            query.exec();
                while (query.next()) {
                    QSqlRecord tempRecoord = query.record();
                    for (int i = 0; i <tempRecoord.count(); i++) {
                        databaseNames << record.value(i).toString();
                    }
                    list.append(list);
                }
        } else {
             message.setText("Error: the connection doesn't exist!");
        }
        return list;
    }

    QString Database::getDatabaseConnectionOptions () const {
        return databaseConnectionOptions;
    }

    QVector <QStringList> Database::getDatabaseDriver(){
        databaseDriver.push_back(database.drivers());
        return databaseDriver;
    }

    QVector <QString> Database::getTablesNames () {
        QMessageBox message;
        QVector <QString> tableNames;
        if (getOpened() == true) {
            query.prepare("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES;");
            query.exec();
                if (!query.lastError().isValid()) {
                    while (query.next()) {
                        QSqlRecord tempRecoord = query.record();
                        for (int i = 0; i <tempRecoord.count(); i++) {
                            tableNames << record.value(i).toString();
                        }
                    }
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
             message.setText("Error: the connection doesn't exist!");
        }
        return tableNames;
   }

    QString Database::getLastQuery () const {
        return query.lastQuery();
    }

    QString Database::getConnectedDatabaseName () const {
        return databaseName;
    }

    QString Database::getDatabaseHost () const {
        return databaseHost;
    }

    unsigned short int Database::getDatabasePort () const {
        return databasePort;
    }

    bool Database::getOpened() const {
            return opened;
    }


    bool Database::createNewDatabase ( QString databaseName) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            query.prepare("CREATE DATABASE :DATABASE_NAME;");
            query.bindValue(":DATABASE_NAME", databaseName);
            query.exec();
                if ( !query.lastError ().isValid () ) {
                    temporary = true;
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
             message.setText("Error: the connection doesn't exist!");
        }
        return temporary;
    }

    bool Database::createNewTable (QString tableName) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            query.prepare("CREATE TABLE :TABLE_NAME;");
            query.bindValue(":TABLE_NAME", tableName);
            query.exec();
                if (!query.lastError().isValid()) {
                    temporary = true;
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
             message.setText("Error: the connection doesn't exist!");
        }
        return temporary;
    }

    bool Database::addNewColumn (QString tableName, QString columnName, QString dataType) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            query.prepare("ALTER TABLE :TABLE_NAME ADD :COLUMN_NAME :DATA_TYPE;");
            query.bindValue(":TABLE_NAME", tableName);
            query.bindValue(":COLUMN_NAME", columnName);
            query.bindValue(":DATA_TYPE", dataType);
            query.exec();
                if (!query.lastError().isValid()) {
                    temporary = true;
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
            message.setText("Error: the connection doesn't exist!");
        }

        return temporary;
    }

    bool Database::delateColumn(QString tableName, QString columnName) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            query.prepare("ALTER TABLE :TABLE_NAME DROP COLUMN :COLUMN_NAME;");
            query.bindValue(":TABLE_NAME", tableName);
            query.bindValue(":COLUMN_NAME", columnName);
            query.exec();
                if (!query.lastError().isValid()) {
                    temporary = true;
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
            message.setText("Error: the connection doesn't exist!");
        }
        return temporary;
    }

    bool Database::changeNameOfColumn(QString tableName, QString columnName ,QString newName) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            query.prepare("ALTER TABLE :TABLE_NAME RENAME COLUMN :COLUMN_NAME TO :NEW_COLUMN_NAME;");
            query.bindValue(":TABLE_NAME", tableName);
            query.bindValue(":COLUMN_NAME", columnName);
            query.bindValue(":NEW_COLUMN_NAME", newName);
            query.exec();
                if (!query.lastError().isValid()) {
                    temporary = true;
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
            message.setText("Error: the connection doesn't exist!");
        }
        return temporary;
    }

    bool Database::changeDatabaseName (QString oldName, QString newName) {
        bool temporary = false;
        QMessageBox message;
        QVector <QStringList> databaseNames = getDatabaseNamesOnServer();
        if (newName == "") {
            message.setText("Error: invalid Name");
            message.exec();
        } else {
            for (int i = 0; i < databaseNames.size() - 1; i ++) {
                if (!databaseNames[i].contains(newName)) {
                    if (getDatabaseConnectionOptions () == "QMYSQL") {
                        query.prepare("RENAME DATABASE :OLD_DATABASE_NAME TO :NEW_DATABASE_NAME;");
                        query.bindValue(":OLD_DATABASE_NAME", oldName);
                        query.bindValue(":NEW_DATABASE_NAME", newName);
                    } else if (getDatabaseConnectionOptions () == "QODBC") {
                        query.prepare("ALTER DATABASE :OLD_DATABASE_NAME MODIFY NAME = :NEW_DATABASE_NAME;");
                        query.bindValue(":OLD_DATABASE_NAME", oldName);
                        query.bindValue(":NEW_DATABASE_NAME", newName);
                    } else if (getDatabaseConnectionOptions () == "QPSQL") {
                        query.prepare("ALTER DATABASE :OLD_DATABASE_NAME RENAME TO = :NEW_DATABASE_NAME;");
                        query.bindValue(":OLD_DATABASE_NAME", oldName);
                        query.bindValue(":NEW_DATABASE_NAME", newName);
                    } else {
                        message.setText("Error: the driver ist nicht eneable");
                        message.exec();
                    }
                    temporary = true;
                } else {
                    message.setText("Error: invalid Name");
                    message.exec();
                }
            }
            query.exec();
        }
        return temporary;
    }

    bool Database::updateQuery (QString tableName, QVector <QString> columns, QVector <QString> values, QString condition, QString conditionValue) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            for (int i = 0; i < columns.size(); i++) {
                query.prepare("UPDATE :TABLE_NAME SET :COLUMN = :VALUE WHERE :CONDITION = :CONDITION_VALUE;");
                query.bindValue(":TABLE_NAME", tableName);
                query.bindValue(":COLUMN", columns[i]);
                query.bindValue(":VALUE", values[i]);
                query.bindValue(":CONDITION", condition);
                query.bindValue(":CONDITION_VALUE", conditionValue);
                query.exec();
                    if (!query.lastError().isValid()) {
                        temporary = true;
                    } else {
                        message.setText("Error: " + query.lastError ().text());
                        message.exec();
                    }
            }
        } else {
            message.setText("Error: the connection doesn't exist!");
        }
        return temporary;
    }

    bool Database::deleteRow(qint8 rowID, QString tableName) {
        bool temporary = false;
        QMessageBox message;
        if (getOpened() == true) {
            query.prepare("DELETE FROM :TABLE_NAME WHERE :ROW_ID; ");
            query.bindValue(":TABLE_NAME", tableName);
            query.bindValue(":ROW_ID", rowID);
            query.exec();
                if (!query.lastError().isValid()) {
                    temporary = true;
                } else {
                    message.setText("Error: " + query.lastError ().text());
                    message.exec();
                }
        } else {
            message.setText("Error: the connection doesn't exist!");
        }
        return temporary;
    }


}
