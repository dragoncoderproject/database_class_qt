//******************************************************************************************************************************************************************************************************
//*                                                                             Created by DragonCoder
//*                                                                             CREATED ON: 14.02.2020
//* THIS FILE WAS WRITTEN FOR ACCESS TO MANIPULATION FOR DATABASE WITH HELP OF QTFRAMEWORK.
//*
//*
//*
//******************************************************************************************************************************************************************************************************
//*                                                                             CHANGES:
//* 16.02.2020 fine work on the code and changing some SQL statments and usage of preapre and bindValue
//* 18.02.2020 changeDatabaseName, updateQuery, delateRow function finished and some changes in code design
//******************************************************************************************************************************************************************************************************
#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlRecord>
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QMessageBox>
#include <QDebug>
#include <QVector>
#include <QString>
#include <QStringList>



namespace DATABASE {
    class Database {
    private:
        QString                         databaseName;
        QString                         databaseHost;
        QString                         databasePassword;
        QString                         databaseUserName;
        QString                         databaseConnectionOptions;
        unsigned short int              databasePort;
        bool                            opened;

        QVector <QString>               databaseTableNames;
        QVector <QStringList>           databaseDriver;

        QSqlDatabase                    database;
        QSqlQuery                       query;
        QSqlRecord                      record;

    public:
        Database(QString databaseName, QString databaseHost, QString databasePassword, QString databaseUserName, QString databaseConnectionOptions, unsigned short int  databasePort)
            : databaseName (databaseName), databaseHost (databaseHost), databasePassword (databasePassword),
              databaseUserName (databaseUserName), databaseConnectionOptions (databaseConnectionOptions), databasePort (databasePort) {};

        bool                            init ();

        QVector <QStringList>           getDatabaseNamesOnServer ();
        QString                         getDatabaseConnectionOptions () const;
        QVector <QStringList>           getDatabaseDriver ();
        QVector <QString>               getTablesNames ();
        QString                         getLastQuery () const;

        QString                         getConnectedDatabaseName () const;
        QString                         getDatabaseHost () const;
        unsigned short int              getDatabasePort () const;
        bool                            getOpened () const;


        bool                            createNewDatabase (QString databaseName);
        bool                            createNewTable (QString tableName);
        bool                            addNewColumn (QString tableName, QString columnName, QString dataType);
        bool                            delateColumn (QString tableName, QString columnName);
        bool                            changeNameOfColumn (QString tableName, QString newName, QString columnName);


        bool                            changeDatabaseName (QString oldName, QString newName);
        bool                            updateQuery (QString tableName, QVector <QString> columns, QVector <QString> values, QString condition, QString conditionValue);
        bool                            deleteRow (qint8 rowID, QString tableName);



    };
}
#endif // DATABASE_H